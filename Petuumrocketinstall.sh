#!/bin/bash

# A script to download and install Petuum 
# This has been tested on Rocket 
# One should in principle check md5sums to be sure downloaded
# files have not been corrupted. 
# It is assumed you have some experience with the unix shell
# and also with compiling source code using command line
# rather than through an IDE. If you need more information 
# on this, see
# http://software-carpentry.org/v4/

# clear loaded modules
module purge
# load a recent gcc compiler suite
module load openmpi-1.8.4
# First go to your home directory
cd $HOME

# Get Petuum 
export PETUUM=petuum
mkdir $PETUUM
cd $PETUUM
git clone https://github.com/petuum/bosen.git
git clone https://github.com/petuum/strads.git
cd bosen
git clone https://github.com/petuum/third_party.git
cd ..

cd strads
make
cd ../bosen/third_party
make
cd ../../bosen
cp defns.mk.template defns.mk
make
cd ..

# build speech learning example
# follows https://github.com/petuum/bosen/wiki/DNN-for-speech-recognition
#cd bosen
#cd app
#cd third_party
#make
#tar -xvf kaldi-trunk.tar.gz
#cd kaldi-trunk/tools

# build matrix factorization example, follows
# https://github.com/petuum/bosen/wiki/Matrix-Factorization
cd $HOME/$PETUUM/strads/apps/cdmf_release/
make

# build Logistic regression example
# https://github.com/petuum/bosen/wiki/Logistic-regression,-Lasso
cd $HOME/$PETUUM/strads/apps/linear-solver_release/
make

# Build Latent dirichlet allocation
# https://github.com/petuum/bosen/wiki/Latent-Dirichlet-Allocation
cd $HOME/$PETUUM/strads/apps/lda_release/
make

# Build Sparse coding
# https://github.com/petuum/bosen/wiki/Sparse-Coding
cd $HOME/$PETUUM/bosen/app/sparsecoding/
make

# Build MedLDA
# https://github.com/petuum/bosen/wiki/MedLDA 
cd $HOME/$PETUUM/strads/apps/medlda_release/
make