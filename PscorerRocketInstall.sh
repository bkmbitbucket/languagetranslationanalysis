#!/bin/bash

# A script to download and install Pscorer 
# https://github.com/medmediani/pscorer 
#
# This has been tested on Rocket 
# One should in principle check md5sums to be sure downloaded
# files have not been corrupted. 
# It is assumed you have some experience with the unix shell
# and also with compiling source code using command line
# rather than through an IDE. If you need more information 
# on this, see
# http://software-carpentry.org/v4/

# clear loaded modules
module purge
# load a recent gcc compiler suite
module load openmpi-1.8.4
module load python-2.7.3
module load cmake-2.8.12.2

# First go to your home directory
cd $HOME

export PSCORER=pscorer
mkdir $PSCORER
cd $PSCORER

# Get STXXL
# Current development version of STXXL seems not to work
#export STXXL=stxxl
#mkdir $STXXL
#cd $STXXL
#git clone https://github.com/stxxl/stxxl
#mkdir debug; cd debug
#cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_CC_COMPILER=mpicc \
# -DCMAKE_INSTALL_PREFIX=$HOME/$PSCORER/$STXXL/install -DBUILD_STATIC_LIBS=ON $HOME/$PSCORER/$STXXL/stxxl 
#make -j4 && make install
#cd ..
#mkdir release; cd release
#cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=mpicxx -DCMAKE_CC_COMPILER=mpicc \
# -DCMAKE_INSTALL_PREFIX=$HOME/$PSCORER/$STXXL/install -DBUILD_STATIC_LIBS=ON $HOME/$PSCORER/$STXXL/stxxl
#make -j4 && make install
#cd ..
wget http://sourceforge.net/projects/stxxl/files/stxxl/1.3.1/stxxl-1.3.1.tar.gz/download 
mv download stxxl-1.3.1.tar.gz
tar -xvf stxxl-1.3.1.tar.gz
cd stxxl-1.3.1
make config_gnu
sed '3 c #COMPILER_GCC    = mpicxx' make.settings.local > make.settings.local2
sed '4 c #COMPILER_GCC    = mpicxx -std=c++0x' make.settings.local2 > make.settings.local
sed '8 c #USE_PARALLEL_MODE       = yes' make.settings.local > make.settings.local2
rm make.settings.local2 

sed '49 c COMPILER_GCC    ?= mpicxx' make.settings > make.settings2
sed '65 c COMPILER_GCC    ?= mpicxx' make.settings2 > make.settings
rm make.settings2

sed '49 c COMPILER_GCC    ?= mpicxx' make.settings.gnu > make.settings2.gnu
sed '65 c COMPILER_GCC    ?= mpicxx' make.settings2.gnu > make.settings.gnu
rm make.settings2.gnu

cd utils
sed '20 c #include <unistd.h>' mlock.cpp > mlock2.cpp
rm mlock.cpp
mv mlock2.cpp mlock.cpp
cd ..
make library_g++_pmode

cd $HOME/$PSCORER
# Get DEMsort
wget http://algo2.iti.kit.edu/documents/DEM-sort-lib.tar.gz
tar -xvf DEM-sort-lib.tar.gz
mkdir include
mv demsort include

# Get NLopt
wget ab-initio.mit.edu/nlopt/nlopt-2.4.2.tar.gz
tar -xvf nlopt-2.4.2.tar.gz
cd nlopt-2.4.2
sh ./autogen.sh
./configure CC=mpicc CXX=mpicxx --prefix=$HOME/$PSCORER/nloptinstall
make
make install

cd $HOME
cd $PSCORER
git clone https://github.com/medmediani/pscorer.git
cd pscorer
sed '2 c CXX = mpicxx  -Wno-deprecated -fpermissive' make.settings.local > make.settings.local2
sed '4 c STXXL_PATH=$(HOME)/$(PSCORER)/$(STXXL)/stxxl-1.3.1/' make.settings.local2 > make.settings.local
sed '9 c OPTIONS = -I$(HOME)/$(PSCORER)/include/ -I$(HOME)/$(PSCORER)/$(STXXL)/stxxl-1.3.1/include -I$(HOME)/$(PSCORER)/nloptinstall/include' \
make.settings.local > make.settings.local2

rm make.settings.local
mv make.settings.local2 make.settings.local
sed '26 c STXXL_LINKER_OPTIONS += $(OPENMP_OPTIONS) -L$(HOME)/$(PSCORER)/nloptinstall/lib -lnlopt -lm'\
 Makefile > Makefile2
rm Makefile 
mv Makefile2 Makefile
make