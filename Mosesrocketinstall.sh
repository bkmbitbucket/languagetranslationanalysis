#!/bin/bash

# A script to download and install Moses 
# http://www.statmt.org/moses/?n=Development.GetStarted
#
# This has been tested on Rocket 
# One should in principle check md5sums to be sure downloaded
# files have not been corrupted. 
# It is assumed you have some experience with the unix shell
# and also with compiling source code using command line
# rather than through an IDE. If you need more information 
# on this, see
# http://software-carpentry.org/v4/

# clear loaded modules
module purge
# load a recent gcc compiler suite
module load openmpi-1.8.4
module load python-2.7.3
#module load zlib-1.2.8

# First go to your home directory
cd $HOME

# Get Moses 
export MOSES=moses
mkdir $MOSES
cd $MOSES

# Get zlib
wget zlib.net/zlib-1.2.8.tar.gz
tar -xvf zlib-1.2.8.tar.gz

# Get Bzip2
wget bzip.org/1.0.6/bzip2-1.0.6.tar.gz
tar -xvf bzip2-1.0.6.tar.gz

# Get GIZA++
git clone https://github.com/moses-smt/giza-pp.git
cd giza-pp
make
cd ..

# Get cmph
git clone https://github.com/zvelo/cmph
cd cmph
./configure CC=mpicc --prefix=$HOME/$MOSES/cmphinstall
make
make install
cd ..

# Get IRSTLM
git clone https://github.com/luispedro/irstlm
cd irstlm
./regenerate-makefiles.sh
./configure CC=mpicc --prefix=$HOME/$MOSES/irstlminstall
make
make install
cd .. 

# Get Boost
wget http://downloads.sourceforge.net/project/boost/boost/1.60.0/boost_1_60_0.tar.gz
tar zxvf boost_1_60_0.tar.gz
cd boost_1_60_0/
./bootstrap.sh --with-libraries=all 
./b2 -sBZIP2_SOURCE=$HOME/$MOSES/bzip2-1.0.6 -sZLIB_SOURCE=$HOME/$MOSES/zlib-1.2.8  \
-a --prefix=$PWD --libdir=$PWD/lib64  --layout=system link=shared  install

cd ..

git clone https://github.com/moses-smt/mosesdecoder.git
cd mosesdecoder
./bjam link=shared --with-boost=$HOME/$MOSES/boost_1_60_0/ > build.log
mkdir tools
cp $HOME/$MOSES/giza-pp/GIZA++-v2/GIZA++ $HOME/$MOSES/giza-pp/GIZA++-v2/snt2cooc.out \
   $HOME/$MOSES/giza-pp/mkcls-v2/mkcls tools
cd ..

# Get example corpus and test installation
wget http://www.statmt.org/moses/download/sample-models.tgz
tar xzf sample-models.tgz
cd sample-models
srun ../mosesdecoder/bin/moses -f phrase-model/moses.ini < phrase-model/in > out
