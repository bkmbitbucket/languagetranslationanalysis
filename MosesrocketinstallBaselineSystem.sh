#!/bin/bash

# A script to download and install Moses, then
# setup a baseline translation system 
# http://www.statmt.org/moses/?n=Development.GetStarted
#
# This has been tested on Rocket 
# One should in principle check md5sums to be sure downloaded
# files have not been corrupted. 
# It is assumed you have some experience with the unix shell
# and also with compiling source code using command line
# rather than through an IDE. If you need more information 
# on this, see
# http://software-carpentry.org/v4/

# clear loaded modules
module purge
# load a recent gcc compiler suite
module load openmpi-1.8.4
module load python-2.7.3
module load cmake-2.8.12.2

# First go to your home directory
cd $HOME

# Get Moses 
export MOSES=mosesbaseline
mkdir $MOSES
cd $MOSES

# Get zlib
wget zlib.net/zlib-1.2.8.tar.gz
tar -xvf zlib-1.2.8.tar.gz

# Get Bzip2
wget bzip.org/1.0.6/bzip2-1.0.6.tar.gz
tar -xvf bzip2-1.0.6.tar.gz

# Get and build pscorer
export PSCORER=pscorer
mkdir $PSCORER
cd $PSCORER

wget http://sourceforge.net/projects/stxxl/files/stxxl/1.3.1/stxxl-1.3.1.tar.gz/download 
mv download stxxl-1.3.1.tar.gz
tar -xvf stxxl-1.3.1.tar.gz
cd stxxl-1.3.1
make config_gnu
sed '3 c #COMPILER_GCC    = mpicxx' make.settings.local > make.settings.local2
sed '4 c #COMPILER_GCC    = mpicxx -std=c++0x' make.settings.local2 > make.settings.local
sed '8 c #USE_PARALLEL_MODE       = yes' make.settings.local > make.settings.local2
rm make.settings.local2 

sed '49 c COMPILER_GCC    ?= mpicxx' make.settings > make.settings2
sed '65 c COMPILER_GCC    ?= mpicxx' make.settings2 > make.settings
rm make.settings2

sed '49 c COMPILER_GCC    ?= mpicxx' make.settings.gnu > make.settings2.gnu
sed '65 c COMPILER_GCC    ?= mpicxx' make.settings2.gnu > make.settings.gnu
rm make.settings2.gnu

cd utils
sed '20 c #include <unistd.h>' mlock.cpp > mlock2.cpp
rm mlock.cpp
mv mlock2.cpp mlock.cpp
cd ..
make library_g++_pmode

cd $HOME/$MOSES/$PSCORER
# Get DEMsort
wget http://algo2.iti.kit.edu/documents/DEM-sort-lib.tar.gz
tar -xvf DEM-sort-lib.tar.gz
mkdir include
mv demsort include

# Get NLopt
wget ab-initio.mit.edu/nlopt/nlopt-2.4.2.tar.gz
tar -xvf nlopt-2.4.2.tar.gz
cd nlopt-2.4.2
sh ./autogen.sh
./configure CC=mpicc CXX=mpicxx --prefix=$HOME/$PSCORER/nloptinstall
make
make install

cd $HOME/$MOSES/$PSCORER
git clone https://github.com/medmediani/pscorer.git
cd pscorer
sed '2 c CXX = mpicxx  -Wno-deprecated -fpermissive' make.settings.local > make.settings.local2
sed '4 c STXXL_PATH=$(HOME)/$(MOSES)/$(PSCORER)/stxxl-1.3.1/' make.settings.local2 > make.settings.local
sed '9 c OPTIONS = -I$(HOME)/$(MOSES)/$(PSCORER)/include/ -I$(HOME)/$(MOSES)/$(PSCORER)/stxxl-1.3.1/include -I$(HOME)/$(MOSES)/$(PSCORER)/nloptinstall/include' \
make.settings.local > make.settings.local2

rm make.settings.local
mv make.settings.local2 make.settings.local
sed '26 c STXXL_LINKER_OPTIONS += $(OPENMP_OPTIONS) -L$(HOME)/$(MOSES)/$(PSCORER)/nloptinstall/lib -lnlopt -lm'\
 Makefile > Makefile2
rm Makefile 
mv Makefile2 Makefile
make

cd $HOME/$MOSES
# Get GIZA++
git clone https://github.com/moses-smt/giza-pp.git
cd giza-pp
make
cd ..

# Get Boost
wget http://downloads.sourceforge.net/project/boost/boost/1.60.0/boost_1_60_0.tar.gz
tar zxvf boost_1_60_0.tar.gz
cd boost_1_60_0/

./bootstrap.sh --with-libraries=all
./b2 -sBZIP2_SOURCE=$HOME/$MOSES/bzip2-1.0.6 -sZLIB_SOURCE=$HOME/$MOSES/zlib-1.2.8  \
-a --prefix=$PWD --libdir=$PWD/lib64  --layout=system link=shared  install

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/lib64:$HOME/$MOSES/irstlm/lib:$HOME/$MOSES/cmph/lib
export C_SEARCH_PATH=$C_SEARCH_PATH:$PWD/include:$HOME/$MOSES/irstlm/include:$HOME/$MOSES/cmph/include
export C_INCLUDE_PATH=$C_INCLUDE_PATH:$PWD/include:$HOME/$MOSES/irstlm/include:$HOME/$MOSES/cmph/include

cd ..


# Get MGIZA
git clone https://github.com/moses-smt/mgiza
cd mgiza
cd mgizapp
#cp manual-compile/compile.sh .
#sed '5 c LDFLAGS="-dynamic"' compile.sh > compile2.sh
#sed '13 c SRC_DIR=./src' compile2.sh > compile.sh
#sed '14 c BOOST_ROOT=$HOME/$MOSES/boost_1_60_0' compile.sh > compile2.sh
#sed '15 c BOOST_INCLUDE=$BOOST_ROOT/include' compile2.sh > compile.sh
#sed '57 c $GPP -o d4norm $SRC_DIR/d4norm.cxx      $LDFLAGS -I$BOOST_INCLUDE -I$SRC_DIR -L. -lmgiza  -L$BOOST_LIBRARYDIR -lboost_system -lboost_thread -lpthread' compile.sh > compile2.sh
#sed '59 c $GPP -o hmmnorm $SRC_DIR/hmmnorm.cxx    $LDFLAGS -I$BOOST_INCLUDE -I$SRC_DIR -L. -lmgiza  -L$BOOST_LIBRARYDIR -lboost_system -lboost_thread -lpthread' compile2.sh > compile.sh
#sed '61 c $GPP -o mgiza $SRC_DIR/main.cpp         $LDFLAGS -I$BOOST_INCLUDE -I$SRC_DIR -L. -lmgiza  -L$BOOST_LIBRARYDIR -lboost_system -lboost_thread -lpthread -lrt' compile.sh > compile2.sh
#sed '71 c $GPP -o symal $SRC_DIR/symal.cpp        $LDFLAGS -I$BOOST_INCLUDE -I$SRC_DIR -L. -lmgiza  -L$BOOST_LIBRARYDIR -lboost_system -lboost_thread -lpthread' compile2.sh > compile.sh  
# Remove temporary file
#rm compile2.sh
#./compile.sh
export BOOST_LIBRARYDIR=$HOME/$MOSES/boost_1_60_0/lib64/
export BOOST_ROOT=$HOME/$MOSES/boost_1_60_0/
cmake .
make
cp scripts/merge_alignment.py .
cd ..
cd ..
# Get cmph
git clone https://github.com/zvelo/cmph
cd cmph
./configure CC=mpicc --disable-static --prefix=$HOME/$MOSES/cmph
make
make install
cd ..

# Get IRSTLM
#git clone https://github.com/luispedro/irstlm
#cd irstlm
#./regenerate-makefiles.sh
#./configure CC=mpicc --disable-static --prefix=$HOME/$MOSES/irstlm
#make
#make install
#cd .. 

git clone https://github.com/moses-smt/mosesdecoder.git
cd mosesdecoder
./bjam link=shared --with-boost=$HOME/$MOSES/boost_1_60_0/ 

# Move Giza here
mkdir tools
cp $HOME/$MOSES/giza-pp/GIZA++-v2/GIZA++ $HOME/$MOSES/giza-pp/GIZA++-v2/snt2cooc.out \
   $HOME/$MOSES/giza-pp/mkcls-v2/mkcls tools

cd ..
# copy script needed by MGIZA
#cp $HOME/$MOSES/mosesdecoder/scripts/merge_alignment.py 
# Get example corpus
wget http://www.statmt.org/moses/download/sample-models.tgz
tar xzf sample-models.tgz
cd sample-models
srun $HOME/$MOSES/mosesdecoder/bin/moses -f phrase-model/moses.ini < phrase-model/in > out
cd ..

# Get full corpus
mkdir corpus
cd corpus 
wget http://www.statmt.org/wmt13/training-parallel-nc-v8.tgz
tar zxvf training-parallel-nc-v8.tgz
# Tokenize scripts
srun --mem=20000 --time=00:10:00 $HOME/$MOSES/mosesdecoder/scripts/tokenizer/tokenizer.perl -l en < $HOME/$MOSES/corpus/training/news-commentary-v8.fr-en.en > $HOME/$MOSES/corpus/news-commentary-v8.fr-en.tok.en

srun --mem=20000 --time=00:10:00 $HOME/$MOSES/mosesdecoder/scripts/tokenizer/tokenizer.perl -l fr < $HOME/$MOSES/corpus/training/news-commentary-v8.fr-en.fr > $HOME/$MOSES/corpus/news-commentary-v8.fr-en.tok.fr
# Train truecaser
srun --mem=20000 --time=00:30:00 $HOME/$MOSES/mosesdecoder/scripts/recaser/train-truecaser.perl --model $HOME/$MOSES/corpus/truecase-model.en --corpus $HOME/$MOSES/corpus/news-commentary-v8.fr-en.tok.en

srun --mem=20000 --time=00:30:00 $HOME/$MOSES/mosesdecoder/scripts/recaser/train-truecaser.perl --model $HOME/$MOSES/corpus/truecase-model.fr --corpus $HOME/$MOSES/corpus/news-commentary-v8.fr-en.tok.fr
# Perform truecasing
srun --mem=20000 --time=00:30:00 $HOME/$MOSES/mosesdecoder/scripts/recaser/truecase.perl --model $HOME/$MOSES/corpus/truecase-model.en < $HOME/$MOSES/corpus/news-commentary-v8.fr-en.tok.en > $HOME/$MOSES/corpus/news-commentary-v8.fr-en.true.en

srun --mem=20000 --time=00:30:00 $HOME/$MOSES/mosesdecoder/scripts/recaser/truecase.perl --model $HOME/$MOSES/corpus/truecase-model.fr < $HOME/$MOSES/corpus/news-commentary-v8.fr-en.tok.fr > $HOME/$MOSES/corpus/news-commentary-v8.fr-en.true.fr
# Clean corpus
srun --mem=20000 --time=00:30:00  $HOME/$MOSES/mosesdecoder/scripts/training/clean-corpus-n.perl $HOME/$MOSES/corpus/news-commentary-v8.fr-en.true fr en $HOME/$MOSES/corpus/news-commentary-v8.fr-en.clean 1 80
# Language Model Training
cd ..
mkdir lm
cd lm
srun --mem=20000 --time=00:30:00 $HOME/$MOSES/mosesdecoder/bin/lmplz -o 3 < $HOME/$MOSES/corpus/news-commentary-v8.fr-en.true.en > news-commentary-v8.fr-en.arpa.en

# Binarize model
srun --mem=20000 --time=00:30:00 $HOME/$MOSES/mosesdecoder/bin/build_binary news-commentary-v8.fr-en.arpa.en news-commentary-v8.fr-en.blm.en
# Check model works
echo "is this an English sentence ?"                       \
   | $HOME/$MOSES/mosesdecoder/bin/query news-commentary-v8.fr-en.blm.en
cd ..

# Start first 5 steps of regular training procedure 
mkdir $HOME/$MOSES/working
cd $HOME/$MOSES/working
cp $HOME/$MOSES/mgiza/mgizapp/bin/mkcls $HOME/$MOSES/mgiza/mgizapp/bin/snt2cooc $HOME/$MOSES/mgiza/mgizapp/bin/mgiza $HOME/$MOSES/mgiza/mgizapp/scripts/merge_alignment.py $PWD
srun --mem=20000  --time=04:00:00 $HOME/$MOSES/mosesdecoder/scripts/training/train-model.perl -mgiza -mgiza-cpus 20 -first-step 1 -last-step 5  -root-dir train \
 -corpus $HOME/$MOSES/corpus/news-commentary-v8.fr-en.clean  -f fr -e en -alignment grow-diag-final-and -reordering msd-bidirectional-fe \
 -lm 0:3:$HOME/$MOSES/lm/news-commentary-v8.fr-en.blm.en:8 -external-bin-dir $PWD  > training.out

# Use parallel scoring, first uncompress file
cd train
cd model
gunzip extract.o.sorted.gz
cd ..
cd ..
wget https://bitbucket.org/bkmbitbucket/languagetranslationanalysis/raw/428ab54599091f12a679ea1deacd384495f57a25/runpscorer
mkdir temp
sbatch runpscorer

# Finish training procedure
